package ua.edu.chmnu.fks.oop.database.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.edu.chmnu.fks.oop.database.dao.QuotesDao;
import ua.edu.chmnu.fks.oop.database.exceptions.DaoException;
import ua.edu.chmnu.fks.oop.database.model.Post;
import ua.edu.chmnu.fks.oop.database.model.Quotes;
import ua.edu.chmnu.fks.oop.database.model.User;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class QuotesServiceTest {
    @Mock
    private QuotesDao quotesDao;

    @InjectMocks
    private QuotesService quotesService;

    @Mock
    private Quotes quotes;

    @Mock
    private Post post;

    @Mock
    private User user;

    @Test
    public void shouldSuccessCreateQuotes() {
        when(quotesDao.create(any(Quotes.class))).thenReturn(quotes);
        Quotes quotes2 = quotesService.createQuotes(quotes);
        assertNotNull(quotes2);
    }

    @Test(expected = DaoException.class)
    public void shouldFailCreateQuotes() {
        when(quotesDao.create(any(Quotes.class))).thenThrow(new DaoException());
        Quotes quotes2 = quotesService.createQuotes(quotes);
        assertNull(quotes2);
    }

    @Test
    public void shouldSuccessUpdateQuotes() {
        when(quotesDao.update(any(Quotes.class))).thenReturn(quotes);
        Quotes quotes2 = quotesService.updateQuotes(quotes);
        assertEquals(quotes, quotes2);
        assertNotNull(quotes2);
    }

    @Test(expected = DaoException.class)
    public void shouldFailUpdateQuotes() {
        when(quotesDao.update(any(Quotes.class))).thenThrow(new DaoException());
        Quotes quotes2 = quotesService.updateQuotes(quotes);
        assertNotEquals(quotes, quotes2);
        assertNull(quotes2);
    }

    @Test
    public void shouldSuccessDeleteQuotes() {
        when(quotesDao.delete(anyLong())).thenReturn(1);
        assertEquals(1, quotesService.deleteQuotes(5L));
    }

    @Test(expected = DaoException.class)
    public void shouldFailDeleteQuotes() {
        when(quotesDao.delete(anyLong())).thenThrow(DaoException.class);
        quotesService.deleteQuotes(100L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailDeleteQuotesPassedWrongArgument() {
        quotesService.deleteQuotes(-8L);
    }

    @Test
    public void shouldSuccessReadQuotes() {
        Quotes quotes2 = quotesListReader().get(2);
        when(quotesDao.read(2L)).thenReturn(quotes2);

        Quotes quotes3 = quotesService.readQuotes(2L);

        assertEquals(quotes2, quotes3);
        assertNotNull(quotes3);
    }

    @Test(expected = DaoException.class)
    public void shouldFailReadQuotes() {
        when(quotesDao.read(118L)).thenThrow(DaoException.class);
        Quotes quotes2 = quotesService.readQuotes(118L);
        assertNull(quotes2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailReadQuotesPassedWrongArgument() {
        Quotes quotes2 = quotesService.readQuotes(-77L);
        assertNull(quotes2);
    }

    @Test
    public void shouldSuccessReadQuotesPagination() {
        List<Quotes> list = quotesListReader().subList(0, 4);
        when(quotesDao.read(1, 4)).thenReturn(list);

        List<Quotes> quotes = quotesService.readQuotes(1, 4);

        assertEquals(list, quotes);
        assertNotNull(quotes);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailReadQuotesPagination() {
        quotesService.readQuotes(-2, -5);
    }

    @Test
    public void shouldSuccessReadAllQuotes() {
        List <Quotes> list = quotesListReader();
        when(quotesDao.readAll()).thenReturn(list);

        List<Quotes> quotes = quotesService.readAllQuotes();
        assertEquals(list, quotes);
        assertNotNull(quotes);
    }

    @Test(expected = DaoException.class)
    public void shouldFailReadAllQuotes() {
        List <Quotes> list = quotesListReader();
        when(quotesDao.readAll()).thenThrow(DaoException.class);

        List<Quotes> quotes = quotesService.readAllQuotes();
        assertNotEquals(list, quotes);
        assertNull(quotes);
    }

    private List<Quotes> quotesListReader() {
        return Arrays.asList(
                Quotes.builder()
                        .title("Quotes 1")
                        .user(user)
                        .post(post)
                        .build(),

                Quotes.builder()
                        .title("Quotes 2")
                        .user(user)
                        .post(post)
                        .build(),

                Quotes.builder()
                        .title("Quotes 3")
                        .user(user)
                        .post(post)
                        .build(),

                Quotes.builder()
                        .title("Quotes 4")
                        .user(user)
                        .post(post)
                        .build(),

                Quotes.builder()
                        .title("Quotes 5")
                        .user(user)
                        .post(post)
                        .build()
        );
    }
}