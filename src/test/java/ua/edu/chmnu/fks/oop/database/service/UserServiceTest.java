package ua.edu.chmnu.fks.oop.database.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import ua.edu.chmnu.fks.oop.database.dao.UserDao;
import ua.edu.chmnu.fks.oop.database.exceptions.DaoException;
import ua.edu.chmnu.fks.oop.database.model.User;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class UserServiceTest {
    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserService userService;

    @Mock
    User user;

    @Test
    public void shouldSuccessCreateUser() {
        when(userDao.create(any(User.class))).thenReturn(user);
        User user2 = userService.createUser(user);
        assertNotNull(user2);
    }

    @Test(expected = DaoException.class)
    public void shouldFailCreateUser() {
        when(userDao.create(any(User.class))).thenThrow(new DaoException());
        User user2 = userService.createUser(user);
        assertNull(user2);
    }

    @Test
    public void shouldSuccessUpdateUser() {
        when(userDao.update(any(User.class))).thenReturn(user);
        User user2 = userService.updateUser(user);
        assertEquals(user, user2);
        assertNotNull(user2);
    }

    @Test(expected = DaoException.class)
    public void shouldFailUpdateUser() {
        when(userDao.update(any(User.class))).thenThrow(new DaoException());
        User user2 = userService.updateUser(user);
        assertNotEquals(user, user2);
        assertNull(user2);
    }

    @Test
    public void shouldSuccessDeleteUser() {
        when(userDao.delete(anyLong())).thenReturn(1);
        assertEquals(1, userService.deleteUser(5L));
    }

    @Test(expected = DaoException.class)
    public void shouldFailDeleteUser() {
        when(userDao.delete(anyLong())).thenThrow(DaoException.class);
        userService.deleteUser(100L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailDeleteUserPassedWrongArgument() {
        userService.deleteUser(-8L);
    }

    @Test
    public void shouldSuccessReadUser() {
        User user2 = userListReader().get(0);
        when(userDao.read(0L)).thenReturn(user2);

        User user3 = userService.readUser(0L);

        assertEquals(user2, user3);
        assertNotNull(user3);
    }

    @Test(expected = DaoException.class)
    public void shouldFailReadUser() {
        when(userDao.read(1000L)).thenThrow(DaoException.class);
        User user2 = userService.readUser(1000L);
        assertNull(user2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailReadUserPassedWrongArgument() {
        User user2 = userService.readUser(-3L);
        assertNull(user2);
    }

    @Test
    public void shouldSuccessReadUserPagination() {
        List<User> list = userListReader().subList(0, 2);
        when(userDao.read(1, 2)).thenReturn(list);

        List<User> users = userService.readUser(1, 2);

        assertEquals(list, users);
        assertNotNull(users);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailReadUserPagination() {
        userService.readUser(-5, -5);
    }

    @Test
    public void shouldSuccessReadAllUsers() {
        List <User> list = userListReader();
        when(userDao.readAll()).thenReturn(list);

        List<User> users = userService.readAllUsers();
        assertEquals(list, users);
        assertNotNull(users);
    }

    @Test(expected = DaoException.class)
    public void shouldFailReadAllUsers() {
        List <User> list = userListReader();
        when(userDao.readAll()).thenThrow(DaoException.class);

        List<User> users = userService.readAllUsers();
        assertNotEquals(list, users);
        assertNull(users);
    }

    private List<User> userListReader() {
        return Arrays.asList(
                User.builder()
                        .email("first@gmail.com")
                        .address("korabelov 1")
                        .birthDay(LocalDate.now().minusYears(21L))
                        .build(),

                User.builder()
                        .email("second@gmail.com")
                        .address("korabelov 2")
                        .birthDay(LocalDate.now().minusYears(22L))
                        .build(),

                User.builder()
                        .email("third@gmail.com")
                        .address("korabelov 3")
                        .birthDay(LocalDate.now().minusYears(23L))
                        .build(),

                User.builder()
                        .email("fourth@gmail.com")
                        .address("korabelov 4")
                        .birthDay(LocalDate.now().minusYears(24L))
                        .build(),

                User.builder()
                        .email("fifth@gmail.com")
                        .address("korabelov 5")
                        .birthDay(LocalDate.now().minusYears(25L))
                        .build()
        );
    }
}