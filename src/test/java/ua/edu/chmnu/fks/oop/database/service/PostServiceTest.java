package ua.edu.chmnu.fks.oop.database.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.edu.chmnu.fks.oop.database.dao.PostDao;
import ua.edu.chmnu.fks.oop.database.exceptions.DaoException;
import ua.edu.chmnu.fks.oop.database.model.Post;
import ua.edu.chmnu.fks.oop.database.model.User;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class PostServiceTest {
    @Mock
    private PostDao postDao;

    @InjectMocks
    private PostService postService;

    @Mock
    private Post post;

    @Mock
    private User user;

    @Test
    public void shouldSuccessCreatePost() {
        when(postDao.create(any(Post.class))).thenReturn(post);
        Post post2 = postService.createPost(post);
        assertNotNull(post2);
    }

    @Test(expected = DaoException.class)
    public void shouldFailCreatePost() {
        when(postDao.create(any(Post.class))).thenThrow(new DaoException());
        Post post2 = postService.createPost(post);
        assertNull(post2);
    }

    @Test
    public void shouldSuccessUpdatePost() {
        when(postDao.update(any(Post.class))).thenReturn(post);
        Post post2 = postService.updatePost(post);
        assertEquals(post, post2);
        assertNotNull(post2);
    }

    @Test(expected = DaoException.class)
    public void shouldFailUpdatePost() {
        when(postDao.update(any(Post.class))).thenThrow(new DaoException());
        Post post2 = postService.updatePost(post);
        assertNotEquals(post, post2);
        assertNull(post2);
    }

    @Test
    public void shouldSuccessDeletePost() {
        when(postDao.delete(anyLong())).thenReturn(1);
        assertEquals(1, postService.deletePost(5L));
    }

    @Test(expected = DaoException.class)
    public void shouldFailDeletePost() {
        when(postDao.delete(anyLong())).thenThrow(DaoException.class);
        postService.deletePost(100L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailDeletePostPassedWrongArgument() {
        postService.deletePost(-8L);
    }

    @Test
    public void shouldSuccessReadPost() {
        Post post2 = postListReader().get(1);
        when(postDao.read(1L)).thenReturn(post2);

        Post post3 = postService.readPost(1L);

        assertEquals(post2, post3);
        assertNotNull(post3);
    }

    @Test(expected = DaoException.class)
    public void shouldFailReadPost() {
        when(postDao.read(323L)).thenThrow(DaoException.class);
        Post post2 = postService.readPost(323L);
        assertNull(post2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailReadPostPassedWrongArgument() {
        Post post2 = postService.readPost(-8L);
        assertNull(post2);
    }

    @Test
    public void shouldSuccessReadPostPagination() {
        List<Post> list = postListReader().subList(0, 3);
        when(postDao.read(1, 3)).thenReturn(list);

        List<Post> posts = postService.readPost(1, 3);

        assertEquals(list, posts);
        assertNotNull(posts);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailReadPostPagination() {
        postService.readPost(-3, -9);
    }

    @Test
    public void shouldSuccessReadAllPosts() {
        List <Post> list = postListReader();
        when(postDao.readAll()).thenReturn(list);

        List<Post> posts = postService.readAllPosts();
        assertEquals(list, posts);
        assertNotNull(posts);
    }

    @Test(expected = DaoException.class)
    public void shouldFailReadAllPosts() {
        List <Post> list = postListReader();
        when(postDao.readAll()).thenThrow(DaoException.class);

        List<Post> posts = postService.readAllPosts();
        assertNotEquals(list, posts);
        assertNull(posts);
    }

    private List<Post> postListReader() {
        return Arrays.asList(
                Post.builder()
                        .content("post 1")
                        .title("title 1")
                        .user(user)
                        .build(),

                Post.builder()
                        .content("post 2")
                        .title("title 2")
                        .user(user)
                        .build(),

                Post.builder()
                        .content("post 3")
                        .title("title 3")
                        .user(user)
                        .build(),

                Post.builder()
                        .content("post 4")
                        .title("title 4")
                        .user(user)
                        .build(),

                Post.builder()
                        .content("post 5")
                        .title("title 5")
                        .user(user)
                        .build()
        );
    }
}