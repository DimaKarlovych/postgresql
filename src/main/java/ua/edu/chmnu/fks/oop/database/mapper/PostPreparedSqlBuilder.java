package ua.edu.chmnu.fks.oop.database.mapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PostPreparedSqlBuilder {
    private List<String> tables = new ArrayList<>();

    public static PostPreparedSqlBuilder create(String ... tables) {
        return new PostPreparedSqlBuilder(Arrays.asList(tables));
    }

    public String buildInsert() {
        return String.format("INSERT into %s (user_id, title, content, created_time, updated_time, status) " +
                        "VALUES(?, ?, ?, ?, ?, ?)", tables.get(0));
    }

    public String buildUpdate() {
        return String.format(
                "UPDATE %s set title=?, content=?, updated_time=?, status=? " +
                        "WHERE id=?", tables.get(0));
    }

    public String buildSelect() {
        return String.format(
                "SELECT p.id as post_id, p.title as post_title, p.content as post_content, " +
                "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                "u.status as user_status " +
                "FROM %s p JOIN %s u ON p.user_id = u.id WHERE (p.id=? AND lower(p.status) != 'deleted' AND lower(u.status) != 'deleted')"
                , tables.get(0), tables.get(1));
    }

    public String updatePosts() {
        return String.format("UPDATE %s set status='Deleted' WHERE (user_id=? AND lower(status) != 'deleted')", tables.get(0));
    }
}
