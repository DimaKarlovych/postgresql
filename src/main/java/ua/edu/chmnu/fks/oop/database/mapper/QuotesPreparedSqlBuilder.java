package ua.edu.chmnu.fks.oop.database.mapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class QuotesPreparedSqlBuilder {
    private List<String> tables = new ArrayList<>();

    public static QuotesPreparedSqlBuilder create(String ... tables) {
        return new QuotesPreparedSqlBuilder(Arrays.asList(tables));
    }

    public String buildSelect() {
        return String.format(
                "SELECT q.id as quotes_id, q.title as quotes_title, q.created_time as quotes_created_time, q.status as quotes_status, " +
                "p.id as post_id, p.title as post_title, p.content as post_content, " +
                "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                "u.status as user_status " +
                "FROM %s q " +
                "JOIN %s p ON q.post_id = p.id " +
                "JOIN %s u ON u.id = q.user_id " +
                "WHERE (q.id=? AND lower(u.status) != 'deleted' AND lower(p.status) != 'deleted' AND lower(q.status) != 'deleted')"
                , tables.get(0), tables.get(1), tables.get(2));
    }

    public String buildInsert() {
        return String.format("INSERT into %s (user_id, post_id, title, created_time, status) " +
                "VALUES(?, ?, ?, ?, ?)", tables.get(0));
    }

    public String buildUpdate() {
        return String.format("UPDATE %s set title=?, created_time=?, status=? " +
                "WHERE id=?", tables.get(0));
    }

    public String deleteQuotesByPostId() {
        return String.format("UPDATE %s set status='Deleted' WHERE (post_id=? AND lower(status) != 'deleted')", tables.get(0));
    }

    public String deleteQuotesByUserId() {
        return String.format("Update %s set status='Deleted' WHERE (user_id=? AND lower(status) != 'deleted')", tables.get(0));
    }
}
