package ua.edu.chmnu.fks.oop.database.dao.impl.jdbc;

import ua.edu.chmnu.fks.oop.database.dao.UserDao;
import ua.edu.chmnu.fks.oop.database.exceptions.DaoException;
import ua.edu.chmnu.fks.oop.database.mapper.*;
import ua.edu.chmnu.fks.oop.database.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends GenericDaoImpl<User, Long> implements UserDao {
    private final Connection connection;
    private final UserMapper userMapper;
    private final LocalDateTimeMapper dateTimeMapper;
    private final LocalDateMapper dateMapper;

    public UserDaoImpl(Connection connection, UserMapper userMapper, LocalDateTimeMapper dateTimeMapper, LocalDateMapper dateMapper) {
        this.connection = connection;
        this.userMapper = userMapper;
        this.dateTimeMapper = dateTimeMapper;
        this.dateMapper = dateMapper;
    }

    @Override
    public String tableName() {
        return "users";
    }

    @Override
    public Connection connection() {
        return this.connection;
    }

    @Override
    public User create(User user) throws DaoException {
        String sql = UserPreparedSqlBuilder.create(tableName()).buildInsert();
        try(PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPhone());
            statement.setTimestamp(3, dateMapper.convertTo(user.getBirthDay()));
            statement.setString(4, user.getAddress());
            statement.setString(5, user.getStatus().toString());
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not inserted user record");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getLong(1));
                } else {
                    throw new DaoException("Could not insert user record with generation key");
                }
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(e);
            }
            throw new RuntimeException(e);
        }
        return user;
    }

    @Override
    public User update(User user) throws DaoException {
        String sql = UserPreparedSqlBuilder.create(tableName()).buildUpdate();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPhone());
            statement.setTimestamp(3, dateMapper.convertTo(user.getBirthDay()));
            statement.setString(4, user.getAddress());
            statement.setString(5, user.getStatus().toString());
            statement.setLong(6, user.getId());
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not update user record");
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException(e);
        }
        return user;
    }

    @Override
    public int delete(Long id) throws DaoException {
        List<Integer> list = new ArrayList<>();

        String sql = UserPreparedSqlBuilder.create(tableName(), "public.post", "public.quotes").buildDelete();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not delete user record");
            }
            String sql2 = PostPreparedSqlBuilder.create("public.post").updatePosts();
            try(PreparedStatement statement2 = connection.prepareStatement(sql2)) {
                statement2.setLong(1, id);
                statement2.executeUpdate();

                String sql3 = "SELECT id FROM public.post WHERE user_id="+id;
                try(PreparedStatement statement3 = connection.prepareStatement(sql3)) {
                    try(ResultSet resultSet = statement3.executeQuery()) {
                        while (resultSet.next()) {
                            int ID = resultSet.getInt("id");
                            String sql4 = QuotesPreparedSqlBuilder.create("public.quotes").deleteQuotesByPostId();
                            try(PreparedStatement statement4 = connection.prepareStatement(sql4)) {
                                statement4.setLong(1, ID);
                                statement4.executeUpdate();
                            }
                        }
                    }
                    String sql5 = QuotesPreparedSqlBuilder.create("public.quotes").deleteQuotesByUserId();
                    try(PreparedStatement statement5 = connection.prepareStatement(sql5)) {
                        statement5.setLong(1, id);
                        statement5.executeUpdate();
                    }
                }
            }
            connection.commit();
            return result;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            System.out.println(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public User read(Long id) throws RuntimeException {
        String sql = UserPreparedSqlBuilder.create(tableName()).buildSelect();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            try(ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    connection.commit();
                    return userMapper.convertFrom(result);
                }
                throw new DaoException("Could not find user by id=" + id);
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> read(int page, int size) throws DaoException {
        String sql = String.format("SELECT id as user_id, email as user_email, phone as user_phone, " +
                                   "birth_day as user_birth_day, address as user_address, status as user_status " +
                                    "FROM %s WHERE lower(status) != 'deleted' LIMIT ? OFFSET ?", tableName());
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, page);
            statement.setLong(2, size);
            try(ResultSet result = statement.executeQuery()) {
                List<User> listResult = new ArrayList<>();
                while (result.next()) {
                    listResult.add(userMapper.convertFrom(result));
                }
                connection.commit();
                return listResult;
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> readAll() throws DaoException {
        String sql = String.format("SELECT id as user_id, email as user_email, phone as user_phone,  " +
                        "birth_day as user_birth_day, address as user_address, status as user_status FROM %s " +
                        "WHERE lower(status) != 'deleted' ", tableName());
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            try(ResultSet result = statement.executeQuery()) {
                List<User> listResult = new ArrayList<>();
                while (result.next()) {
                    listResult.add(userMapper.convertFrom(result));
                }
                connection.commit();
                return listResult;
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException(e);
        }
    }
}
