package ua.edu.chmnu.fks.oop.database.service;

import ua.edu.chmnu.fks.oop.database.dao.PostDao;
import ua.edu.chmnu.fks.oop.database.model.Post;

import java.util.List;

public class PostService {
    PostDao postDao;

    public PostService(PostDao postDao) {
        this.postDao = postDao;
    }

    public Post createPost(Post post) {
        return postDao.create(post);
    }

    public Post updatePost(Post post) {
        return postDao.update(post);
    }

    public int deletePost(Long id) {
        if(id < 0) {
            throw new IllegalArgumentException();
        }

        return postDao.delete(id);
    }

    public Post readPost(Long id) {
        if(id < 0) {
            throw new IllegalArgumentException();
        }

        return postDao.read(id);
    }

    public List<Post> readPost(int page, int size) {
        if(page < 0 && size < 0) {
            throw new IllegalArgumentException();
        }

        return postDao.read(page, size);
    }

    public List<Post> readAllPosts() {
        return postDao.readAll();
    }
}
