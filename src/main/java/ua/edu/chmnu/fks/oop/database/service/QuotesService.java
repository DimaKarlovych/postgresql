package ua.edu.chmnu.fks.oop.database.service;

import ua.edu.chmnu.fks.oop.database.dao.QuotesDao;
import ua.edu.chmnu.fks.oop.database.model.Quotes;

import java.util.List;

public class QuotesService {
    QuotesDao quotesDao;

    public QuotesService(QuotesDao quotesDao) {
        this.quotesDao = quotesDao;
    }

    public Quotes createQuotes(Quotes quotes) {
        return quotesDao.create(quotes);
    }

    public Quotes updateQuotes(Quotes quotes) {
        return quotesDao.update(quotes);
    }

    public int deleteQuotes(Long id) {
        if(id < 0) {
            throw new IllegalArgumentException();
        }

        return quotesDao.delete(id);
    }

    public Quotes readQuotes(Long id) {
        if(id < 0) {
            throw new IllegalArgumentException();
        }

        return quotesDao.read(id);
    }

    public List<Quotes> readQuotes(int page, int size) {
        if(page < 0 && size < 0) {
            throw new IllegalArgumentException();
        }

        return quotesDao.read(page, size);
    }

    public List<Quotes> readAllQuotes() {
        return quotesDao.readAll();
    }
}
