package ua.edu.chmnu.fks.oop.database.dao;

import ua.edu.chmnu.fks.oop.database.model.Quotes;

public interface QuotesDao extends GenericDao<Quotes, Long> {
}
