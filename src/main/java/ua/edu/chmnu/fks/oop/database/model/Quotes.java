package ua.edu.chmnu.fks.oop.database.model;

import lombok.*;
import ua.edu.chmnu.fks.oop.database.utils.Status;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Quotes {

    private Long id;

    @EqualsAndHashCode.Include
    private String title;

    private LocalDateTime createdTime;

    private Post post;

    private User user;

    private Status status;
}
