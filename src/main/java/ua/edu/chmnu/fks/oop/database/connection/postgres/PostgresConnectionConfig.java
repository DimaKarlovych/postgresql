package ua.edu.chmnu.fks.oop.database.connection.postgres;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.edu.chmnu.fks.oop.database.connection.ConnectionConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Setter
@AllArgsConstructor
@Builder
public class PostgresConnectionConfig implements ConnectionConfig {
    private String host;
    private int port;
    private String user;
    private String password;
    private String database;

    @Override
    public String jdbcUrl() {
        return String.format("jdbc:postgresql://%s:%d/%s", host, port, database);
    }

    @Override
    public String jdbcUser() {
        return this.user;
    }

    @Override
    public String jdbcPassword() {
        return this.password;
    }


    public PostgresConnectionConfig() {
        try(InputStream fis = this.getClass().getResourceAsStream("/db.properties")) {
            Properties property = new Properties();
            property.load(fis);
            String host = property.getProperty("db.host");
            String port = property.getProperty("db.port");
            String user = property.getProperty("db.user");
            String password = property.getProperty("db.password");
            String database = property.getProperty("db.database");

            this.host = host;
            this.port = Integer.parseInt(port);
            this.user = user;
            this.password = password;
            this.database = database;

            System.out.println(host + " " + port + " " + user + " " + password + " " + database);

        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
    }
}
