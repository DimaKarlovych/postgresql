package ua.edu.chmnu.fks.oop.database.mapper;

import ua.edu.chmnu.fks.oop.database.model.Quotes;
import ua.edu.chmnu.fks.oop.database.utils.Status;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QuotesMapper implements Converter<ResultSet, Quotes> {
    private final LocalDateTimeMapper localDateTimeMapper;
    private final UserMapper userMapper;
    private final PostMapper postMapper;

    public QuotesMapper(LocalDateTimeMapper localDateTimeMapper, UserMapper userMapper, PostMapper postMapper) {
        this.localDateTimeMapper = localDateTimeMapper;
        this.userMapper = userMapper;
        this.postMapper = postMapper;
    }

    @Override
    public Quotes convertFrom(ResultSet resultSet) throws RuntimeException {
        try {
            return Quotes.builder()
                    .id(resultSet.getLong("quotes_id"))
                    .title(resultSet.getString("quotes_title"))
                    .createdTime(localDateTimeMapper.convertFrom(resultSet.getTimestamp("quotes_created_time")))
                    .status(Enum.valueOf(Status.class, resultSet.getString("quotes_status").toUpperCase()))
                    .user(userMapper.convertFrom(resultSet))
                    .post(postMapper.convertFrom(resultSet))
                    .build();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResultSet convertTo(Quotes quotes) throws RuntimeException {
        throw new UnsupportedOperationException();
    }
}
