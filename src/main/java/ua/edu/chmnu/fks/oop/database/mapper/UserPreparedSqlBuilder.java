package ua.edu.chmnu.fks.oop.database.mapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserPreparedSqlBuilder {
    private List<String> tables = new ArrayList<>();

    public static UserPreparedSqlBuilder create(String ... tables) {
        return new UserPreparedSqlBuilder(Arrays.asList(tables));
    }

    public String buildInsert() {
        return String.format("INSERT into %s (email, phone, birth_day, address, status) " +
                "VALUES(?, ?, ?, ?, ?)", tables.get(0));
    }

    public String buildUpdate() {
        return String.format(
                "UPDATE %s set email=?, phone=?, birth_day=?, address=?, status=?" +
                        "WHERE id=?", tables.get(0));
    }

    public String buildSelect() {
        return String.format("SELECT id as user_id, email as user_email, phone as user_phone,  birth_day as user_birth_day, " +
                             "address as user_address, status as user_status FROM %s " +
                             "WHERE (id=? AND lower(status) != 'deleted')", tables.get(0));
    }

    public String buildDelete() {
        return String.format("Update %s set status='Deleted' WHERE (id=? AND lower(status) != 'deleted')", tables.get(0), tables.get(1), tables.get(2));
    }
}
