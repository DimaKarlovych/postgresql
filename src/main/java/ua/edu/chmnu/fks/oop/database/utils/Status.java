package ua.edu.chmnu.fks.oop.database.utils;

public enum Status {
    NEW, UPDATED, DELETED;
}
