package ua.edu.chmnu.fks.oop.database.dao.impl.jdbc;

import ua.edu.chmnu.fks.oop.database.dao.PostDao;
import ua.edu.chmnu.fks.oop.database.exceptions.DaoException;
import ua.edu.chmnu.fks.oop.database.mapper.LocalDateTimeMapper;
import ua.edu.chmnu.fks.oop.database.mapper.PostMapper;
import ua.edu.chmnu.fks.oop.database.mapper.PostPreparedSqlBuilder;
import ua.edu.chmnu.fks.oop.database.mapper.QuotesPreparedSqlBuilder;
import ua.edu.chmnu.fks.oop.database.model.Post;
import ua.edu.chmnu.fks.oop.database.model.User;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PostDaoImpl extends GenericDaoImpl<Post, Long> implements PostDao {
    private final Connection connection;
    private final PostMapper postMapper;
    private final LocalDateTimeMapper dateTimeMapper;

    public PostDaoImpl(Connection connection, PostMapper postMapper, LocalDateTimeMapper dateTimeMapper) {
        this.connection = connection;
        this.postMapper = postMapper;
        this.dateTimeMapper = dateTimeMapper;
    }

    @Override
    public String tableName() {
        return "public.post";
    }

    @Override
    public Connection connection() {
        return this.connection;
    }

    @Override
    public Post create(Post post) throws DaoException {
        String sql = PostPreparedSqlBuilder.create(tableName()).buildInsert();

        try(PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, Optional.ofNullable(post.getUser()).map(User::getId).orElse(null));
            statement.setString(2, post.getTitle());
            statement.setString(3, post.getContent());
            statement.setTimestamp(4, dateTimeMapper.convertTo(LocalDateTime.now()));
            statement.setTimestamp(5, dateTimeMapper.convertTo(LocalDateTime.now()));
            statement.setString(6, post.getStatus().toString());
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not inserted post record");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    post.setId(generatedKeys.getLong(1));
                } else {
                    throw new DaoException("Could not insert post record with generation key");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return post;
    }

    @Override
    public Post update(Post post) throws DaoException {
        String sql = PostPreparedSqlBuilder.create(tableName()).buildUpdate();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, post.getTitle());
            statement.setString(2, post.getContent());
            statement.setTimestamp(3, dateTimeMapper.convertTo(LocalDateTime.now()));
            statement.setString(4, post.getStatus().toString());
            statement.setLong(5, post.getId());
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not update post record");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return post;
    }

    @Override
    public int delete(Long id) throws DaoException {
        String sql = String.format("UPDATE %s SET status='Deleted' " +
                "WHERE (id=? AND lower(status) != 'deleted')", tableName());
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not delete post record");
            }
            String sql2 = QuotesPreparedSqlBuilder.create("public.quotes").deleteQuotesByPostId();
            try(PreparedStatement statement2 = connection.prepareStatement(sql2)) {
                statement2.setLong(1, id);
                statement2.executeUpdate();
            }
            connection.commit();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Post read(Long id) throws RuntimeException {
        String sql = PostPreparedSqlBuilder.create(tableName(), "public.users").buildSelect();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            try(ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    connection.commit();
                    return postMapper.convertFrom(result);
                }
                throw new DaoException("Could not find post by id=" + id);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Post> read(int page, int size) throws DaoException {
        String sql = String.format("SELECT p.id as post_id, p.title as post_title, p.content as post_content,  " +
                "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                "u.status as user_status " +
                "FROM %s p " +
                "JOIN %s u ON p.user_id = u.id " +
                "WHERE (lower(p.status) != 'deleted' AND lower(u.status) != 'deleted') " +
                "LIMIT ? OFFSET ?" , tableName(), "public.users");

        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, page);
            statement.setLong(2, size);
            try(ResultSet result = statement.executeQuery()) {
                List<Post> listResult = new ArrayList<>();
                while (result.next()) {
                    listResult.add(postMapper.convertFrom(result));
                }
                connection.commit();
                return listResult;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Post> readAll() throws DaoException {
        String sql = String.format("SELECT p.id as post_id, p.title as post_title, p.content as post_content,  " +
                "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                "u.status as user_status " +
                "FROM %s p " +
                "JOIN %s u ON p.user_id = u.id " +
                "WHERE (lower(p.status) != 'deleted' AND lower(u.status) != 'deleted') ", tableName(), "public.users");
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            try(ResultSet result = statement.executeQuery()) {
                List<Post> listResult = new ArrayList<>();
                while (result.next()) {
                    listResult.add(postMapper.convertFrom(result));
                }
                connection.commit();
                return listResult;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
