package ua.edu.chmnu.fks.oop.database;

import ua.edu.chmnu.fks.oop.database.config.AppConfiguration;
import ua.edu.chmnu.fks.oop.database.connection.ConnectionConfig;
import ua.edu.chmnu.fks.oop.database.connection.ConnectionFactory;
import ua.edu.chmnu.fks.oop.database.connection.postgres.PostgresConnectionConfig;
import ua.edu.chmnu.fks.oop.database.dao.PostDao;
import ua.edu.chmnu.fks.oop.database.dao.QuotesDao;
import ua.edu.chmnu.fks.oop.database.dao.UserDao;
import ua.edu.chmnu.fks.oop.database.dao.impl.jdbc.UserDaoImpl;
import ua.edu.chmnu.fks.oop.database.service.PostService;
import ua.edu.chmnu.fks.oop.database.service.QuotesService;
import ua.edu.chmnu.fks.oop.database.model.Post;
import ua.edu.chmnu.fks.oop.database.model.Quotes;
import ua.edu.chmnu.fks.oop.database.model.User;
import ua.edu.chmnu.fks.oop.database.service.UserService;
import ua.edu.chmnu.fks.oop.database.utils.Status;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

public class DbApp {
    public static void main(String[] args) {
        ConnectionConfig connectionConfig = new PostgresConnectionConfig();

        try(Connection connection = ConnectionFactory.createConnection(connectionConfig)) {
            connection.setAutoCommit(false);
            UserDao userDao = AppConfiguration.getUserDao(connection);
            PostDao postDao = AppConfiguration.getPostDao(connection);
            QuotesDao quotesDao = AppConfiguration.getQuotesDao(connection);

            UserService userService = new UserService(userDao);
            PostService postService = new PostService(postDao);
            QuotesService quotesService = new QuotesService(quotesDao);

            System.out.println(userDao.read(11L));
            User user = User.builder()
                    .address("korabelov 14")
                    .email("14@gmail.com")
                    .phone("0681414114")
                    .birthDay(LocalDate.of(1996,6,14))
                    .status(Status.NEW)
                    .id(14L)
                    .build();

            Post post = Post.builder()
                    .title("post 20")
                    .content("post 20")
                    .status(Status.NEW)
                    .user(user)
                    .build();

            Quotes quotes = Quotes.builder()
                    .title("Quotes to 19 post, from 1 user, post owner - 14 user")
                    .user(user)
                    .post(post)
                    .status(Status.UPDATED)
                    .id(19L)
                    .build();

//            List<Post> posts = Arrays.asList(
//              Post
//              .builder()
//                 .title("Post 1")
//                 .content("Content 1")
//                 .user(user)
//              .build(),
//
//              Post
//                .builder()
//                    .title("Post 2")
//                    .content("Content 2")
//                    .user(user)
//                .build()
//            );

//            posts.stream().map(postDao::create).forEach(System.out::println);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
