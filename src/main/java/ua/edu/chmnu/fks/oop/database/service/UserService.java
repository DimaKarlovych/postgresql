package ua.edu.chmnu.fks.oop.database.service;

import ua.edu.chmnu.fks.oop.database.dao.UserDao;
import ua.edu.chmnu.fks.oop.database.model.User;
import java.util.List;

public class UserService {

    UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public User createUser(User user) { return userDao.create(user); }

    public User updateUser(User user) { return userDao.update(user); }

    public int deleteUser(Long id) {
        if(id < 0) {
            throw new IllegalArgumentException();
        }
        return userDao.delete(id);
    }

    public User readUser(Long id) {
        if(id < 0) {
            throw new IllegalArgumentException();
        }
        return userDao.read(id);
    }

    public List<User> readUser(int page, int size) {
        if(page < 0 && size < 0) {
            throw new IllegalArgumentException();
        }

        return userDao.read(page, size);
    }

    public List<User> readAllUsers() {
        return userDao.readAll();
    }
}
