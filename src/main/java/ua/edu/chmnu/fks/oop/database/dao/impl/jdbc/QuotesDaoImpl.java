package ua.edu.chmnu.fks.oop.database.dao.impl.jdbc;

import ua.edu.chmnu.fks.oop.database.dao.QuotesDao;
import ua.edu.chmnu.fks.oop.database.exceptions.DaoException;
import ua.edu.chmnu.fks.oop.database.mapper.*;
import ua.edu.chmnu.fks.oop.database.model.Post;
import ua.edu.chmnu.fks.oop.database.model.Quotes;
import ua.edu.chmnu.fks.oop.database.model.User;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QuotesDaoImpl extends GenericDaoImpl<Quotes, Long> implements QuotesDao {
    private final Connection connection;
    private final QuotesMapper quotesMapper;
    private final PostMapper postMapper;
    private final LocalDateTimeMapper dateTimeMapper;

    public QuotesDaoImpl(Connection connection, QuotesMapper quotesMapper, PostMapper postMapper, LocalDateTimeMapper dateTimeMapper) {
        this.connection = connection;
        this.quotesMapper = quotesMapper;
        this.postMapper = postMapper;
        this.dateTimeMapper = dateTimeMapper;
    }


    @Override
    public Connection connection() {
        return this.connection;
    }

    @Override
    public String tableName() {
        return "public.quotes";
    }

    @Override
    public Quotes create(Quotes quotes) throws DaoException {
        String sql = QuotesPreparedSqlBuilder.create(tableName()).buildInsert();
        try(PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, Optional.ofNullable(quotes.getUser()).map(User::getId).orElse(null));
            statement.setLong(2, Optional.ofNullable(quotes.getPost()).map(Post::getId).orElse(null));
            statement.setString(3, quotes.getTitle());
            statement.setTimestamp(4, dateTimeMapper.convertTo(LocalDateTime.now()));
            statement.setString(5, quotes.getStatus().toString());
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not inserted quotes record!");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    quotes.setId(generatedKeys.getLong(1));
                } else {
                    throw new DaoException("Could not insert quotes record with generation key");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return quotes;
    }

    @Override
    public Quotes update(Quotes quotes) throws DaoException {
        String sql = QuotesPreparedSqlBuilder.create(tableName()).buildUpdate();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, quotes.getTitle());
            statement.setTimestamp(2, dateTimeMapper.convertTo(LocalDateTime.now()));
            statement.setString(3, quotes.getStatus().toString());
            statement.setLong(4, quotes.getId());
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not update quotes record");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return quotes;
    }

    @Override
    public int delete(Long id) throws DaoException {
        String sql = String.format("UPDATE %s SET status='Deleted' " +
                                "WHERE (id=? AND lower(status) != 'deleted')", tableName());
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            int result = statement.executeUpdate();
            if (result != 1) {
                throw new DaoException("Could not delete non existent quotes record");
            }
            connection.commit();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Quotes read(Long id) throws DaoException {
        String sql = QuotesPreparedSqlBuilder.create(tableName(), "public.post", "public.users").buildSelect();
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            try(ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    Quotes quotes = quotesMapper.convertFrom(result);
                    String sql2 = PostPreparedSqlBuilder.create("public.post", "public.users").buildSelect();
                    try(PreparedStatement userPostStat = connection.prepareStatement(sql2)) {
                        userPostStat.setLong(1, quotes.getPost().getId());
                        try(ResultSet rs2 = userPostStat.executeQuery()) {
                            if (rs2.next()) {
                                Post post = postMapper.convertFrom(rs2);
                                quotes.setPost(post);
                            }
                            else {
                                throw new DaoException("Could not find post for quotes by id=" + id);
                            }
                        }
                    }
                    connection.commit();
                    return quotes;
                } else {
                    throw new DaoException("Could not find quotes by id=" + id);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Quotes> read(int page, int size) throws DaoException {
        List<Quotes> listResult = new ArrayList<>();
        String sql = String.format("SELECT q.id as quotes_id, q.title as quotes_title, q.created_time as quotes_created_time, q.status as quotes_status, " +
                "p.id as post_id, p.title as post_title, p.content as post_content, " +
                "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                "u.status as user_status " +
                "FROM %s q " +
                "JOIN %s p ON q.post_id = p.id " +
                "JOIN %s u ON u.id = q.user_id " +
                "WHERE (lower(u.status) != 'deleted' AND lower(p.status) != 'deleted' AND lower(q.status) != 'deleted') " +
                "LIMIT ? OFFSET ?"
                , tableName(), "public.post", "public.users");

        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, page);
            statement.setLong(2, size);
            try(ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("post_id");
                    Quotes quotes = quotesMapper.convertFrom(result);
                    String sql2 = String.format("SELECT p.id as post_id, p.title as post_title, p.content as post_content, " +
                            "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                            "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                            "u.status as user_status " +
                            "FROM %s p JOIN %s u ON p.user_id = u.id WHERE (p.id=? AND lower(p.status) != 'deleted' AND lower(u.status) != 'deleted')"
                            , "public.post", "public.users");
                    try(PreparedStatement userPostStat = connection.prepareStatement(sql2)) {
                        userPostStat.setInt(1, id);
                        try(ResultSet rs2 = userPostStat.executeQuery()) {
                            if (rs2.next()) {
                                Post post = postMapper.convertFrom(rs2);
                                quotes.setPost(post);
                                listResult.add(quotes);
                            }
                        }
                    }
                }
                connection.commit();
                return listResult;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Quotes> readAll() throws DaoException {
        List<Quotes> listResult = new ArrayList<>();
        String sql = String.format("SELECT q.id as quotes_id, q.title as quotes_title, q.created_time as quotes_created_time, q.status as quotes_status, " +
                        "p.id as post_id, p.title as post_title, p.content as post_content, " +
                        "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                        "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                        "u.status as user_status " +
                        "FROM %s q " +
                        "JOIN %s p ON q.post_id = p.id " +
                        "JOIN %s u ON u.id = q.user_id " +
                        "WHERE (lower(u.status) != 'deleted' AND lower(p.status) != 'deleted' AND lower(q.status) != 'deleted') "
                , tableName(), "public.post", "public.users");

        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            try(ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("post_id");
                    Quotes quotes = quotesMapper.convertFrom(result);
                    String sql2 = String.format("SELECT p.id as post_id, p.title as post_title, p.content as post_content, " +
                                    "p.created_time as post_created_time, p.updated_time as post_updated_time, p.status as post_status, " +
                                    "u.id as user_id, u.email as user_email, u.phone as user_phone, u.birth_day as user_birth_day, u.address as user_address, " +
                                    "u.status as user_status " +
                                    "FROM %s p JOIN %s u ON p.user_id = u.id WHERE (p.id=? AND lower(p.status) != 'deleted' AND lower(u.status) != 'deleted')"
                            , "public.post", "public.users");
                    try(PreparedStatement userPostStat = connection.prepareStatement(sql2)) {
                        userPostStat.setInt(1, id);
                        try(ResultSet rs2 = userPostStat.executeQuery()) {
                            if (rs2.next()) {
                                Post post = postMapper.convertFrom(rs2);
                                quotes.setPost(post);
                                listResult.add(quotes);
                            }
                        }
                    }
                }
                connection.commit();
                return listResult;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
